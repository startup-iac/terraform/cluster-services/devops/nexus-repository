variable "namespace" {
  type    = string
}

variable "chart_version" {
  type    = string
  default = "66.0.0"
}

variable "dns_name" {
  type    = string
}

variable "cluster_issuer_name" {
  type    = string
}

variable "subject_organizations" {
  type    = string
}

variable "subject_organizationalunits" {
  type    = string
}

variable "iam_dns_name" {
  type    = string
}

variable "iam_client_secret" {
  type    = string
}